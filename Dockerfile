FROM python:3.9-slim-buster

LABEL Name="Python Flask Demo App" Version=1.4.2
RUN apt-get update && apt-get install -y gcc python3-dev
RUN mkdir -p /app
WORKDIR /app
COPY . /app
RUN pip install --no-cache-dir -r /app/requirements.txt
