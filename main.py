from fastapi import FastAPI, Depends, Request

app = FastAPI()


@app.get('/')
def index(request: Request):
    client_host = request.client.host
    return {"ip": client_host}

